username	= sessaidi

vm_name		= sandbox-lfs-11.1
vm_ostype	= Ubuntu_64
vm_folder	= .
vm_cpus		= 4
vm_memory	= 8192
vm_vmemory	= 8

iso_name	= ubuntu-20.04.iso
iso_url		= https://releases.ubuntu.com/20.04/ubuntu-20.04.4-desktop-amd64.iso 

.PHONY: all
all: mount_lfs

.PHONY: mount_lfs
mount_lfs: prepare_vm
	@say "mount l f s disk"
	@VBoxManage createhd						\
		--filename $(username)-disk.vdi			\
		--size 20480							\
		--format VDI
	#
	@VBoxManage storageattach $(vm_name)		\
		--storagectl "SATA Controller"			\
		--port 1								\
		--type hdd								\
		--medium $(username)-disk.vdi
	@VBoxManage startvm $(vm_name) --type headless
	@VBoxManage guestproperty wait $(vm_name) /VirtualBox/GuestInfo/OS/NoLoggedInUsers
	@say "You can now run, the command make build"


.PHONY: prepare_vm
prepare_vm: apply_modification
	@say "install operating system on virtual machine"
	@VBoxManage unattended install $(vm_name)	\
		--iso $(iso_name)						\
		--user me								\
		--full-user-name me						\
		--password me							\
		--install-additions						\
		--time-zone=UTC
	@VBoxManage startvm $(vm_name) --type headless
	@VBoxManage guestproperty wait $(vm_name) /VirtualBox/GuestInfo/OS/NoLoggedInUsers
	@VBoxManage controlvm $(vm_name) acpipowerbutton
	@sleep 10

.PHONY: apply_modification
apply_modification: attach_storage
	@say "apply modification on virtual machine"
	@VBoxManage modifyvm $(vm_name)				\
		--cpus $(vm_cpus)						\
		--memory $(vm_memory)					\
		--vram $(vm_vmemory)					\
		--graphicscontroller VMSVGA				\
		--nic1 nat								\
		--boot1 dvd								\
		--boot2 disk							\
		--boot3 none							\
		--boot4 none							\
		--natpf1 "ssh,tcp,,3022,,22"

.PHONY: attach_storage
attach_storage: create_hd
	@say "attach storage"
	@VBoxManage storageattach $(vm_name)		\
		--storagectl "IDE Controller"			\
		--port 1								\
		--device 0								\
		--type dvddrive							\
		--medium $(iso_name)
	#
	@VBoxManage storageattach $(vm_name)		\
		--storagectl "SATA Controller"			\
		--port 0								\
		--device 0								\
		--type hdd								\
		--medium $(vm_name)-disk.vdi

.PHONY: create_hd
create_hd: prepare_shared_folder
	@say "create hard disk"
	@VBoxManage createhd						\
		--filename $(vm_name)-disk.vdi			\
		--size 20480							\
		--format VDI

.PHONY: prepare_shared_folder
prepare_shared_folder: create_controllers
	@say "prepare shared folders"
	@[[ ! -d storage ]] && mkdir storage || true;
	#
	@VBoxManage sharedfolder add $(vm_name)		\
		--name "storage"						\
		--hostpath "storage"					\
		--automount
	#
	@VBoxManage sharedfolder add $(vm_name)		\
		--name "scripts"						\
		--hostpath "scripts"					\
		--automount

.PHONY: create_controllers
create_controllers: create_vm
	@say "prepare controllers"
	@VBoxManage storagectl $(vm_name)			\
		--name "SATA Controller"				\
		--add sata								\
		--controller IntelAhci
	#
	@VBoxManage storagectl $(vm_name)			\
		--name "IDE Controller"					\
		--add ide								\
		--controller PIIX4

.PHONY: create_vm
create_vm: get_iso
	@say "create virtual machine"
	@VBoxManage createvm						\
		--name $(vm_name)						\
		--ostype $(vm_ostype)					\
		--register								\
		--basefolder=$(vm_folder)

.PHONY: get_iso
get_iso:
	@if [ ! -f $(iso_name) ]; 					\
	then 										\
		say "get iso disk"; \
		wget -O $(iso_name) $(iso_url) ; 		\
	fi

.PHONY: vm_up
vm_up:
	@if [[ ! $(shell VBoxManage showvminfo --machinereadable $(vm_name) | grep "VMState=") =~ "running" ]]; \
	then \
		VBoxManage startvm $(vm_name) --type headless; \
		VBoxManage guestproperty wait $(vm_name) /VirtualBox/GuestInfo/OS/NoLoggedInUsers; \
	fi

.PHONY: vm_halt
vm_halt:
	@if [[ $(shell VBoxManage showvminfo --machinereadable $(vm_name) | grep "VMState=") =~ "running" ]];\
	then \
		VBoxManage controlvm $(vm_name) acpipowerbutton; \
	fi

build:
	@say "Start build lfs disk"
	@make -C scripts

.PHONY: clean
clean:
	@rm ~/.iso/$(ISO_NAME).iso

.PHONY: fclean
fclean: vm_halt

.PHONY: re
re: fclean all
